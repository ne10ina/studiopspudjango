from django.conf.urls import url
from django.urls import path

from calendarApp.views import *

urlpatterns = [
    url(r'unsubscribe/$', unsubscribe, name="unsubscribe"),
    path('year=<int:year>&month=<int:month>', calendarParametrized, name='calendarParametrized'),
    path('', calendar, name='calendar'),
]