FROM python:3.11.4-slim-buster

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY . /app

EXPOSE 80
ENTRYPOINT ["python3"]
CMD ["manage.py", "runserver", "0.0.0.0:80"]