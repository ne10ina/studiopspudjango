from django.shortcuts import render
from django.template import RequestContext

# Create your views here.
from structs import buildHeaderNavItems, linkType


def mainView(request):
    return render(request, 'index.html', {'headerNav': buildHeaderNavItems(['ПГГПУ-Онлайн', 'Направления проекта', 'Подключись к онлайну', 'Отзывы о проекте', 'Контакты', 'Инструкции', 'Таблица записи'],
                                                                           ['about', 'proms', 'service', 'review', 'contacts', 'features', 'calendar'],
                                                                           [linkType.div, linkType.div, linkType.div, linkType.div, linkType.div, linkType.pattern, linkType.pattern])})

def featuresView(request):
    return render(request, 'features.html', {'headerNav': buildHeaderNavItems(['Главная страница', 'Инструкции', 'Таблица записи'],
                                                                           ['index', 'features', 'calendar'],
                                                                           [linkType.pattern, linkType.pattern, linkType.pattern])})